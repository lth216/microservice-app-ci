variables:
  RECOMMENDATION_SERVICE_NAME: recommendationservice
  JFROG_PROJECT_SNAPSHOT: ${JFROG_URL}/artifactory/onl-boutique-repo-dev/snapshot/${RECOMMENDATION_SERVICE_NAME}/${CI_COMMIT_SHORT_SHA}
  BUILD_ARTIFACT: ${RECOMMENDATION_SERVICE_NAME}-artifact.zip
  SONAR_REPORT: ${RECOMMENDATION_SERVICE_NAME}-sonar-scan.html
  SNYK_REPORT: ${RECOMMENDATION_SERVICE_NAME}-snyk-scan.html
  TRIVY_REPORT: ${RECOMMENDATION_SERVICE_NAME}-image-scan.html
  FULL_IMAGE: ${PORTUS_URL}/dev/${RECOMMENDATION_SERVICE_NAME}:${CI_COMMIT_SHORT_SHA}

Sonarqube_Scan_Recommendationservice:
    image: 
        name: sonarsource/sonar-scanner-cli:5.0
        entrypoint: [""]
    stage: static-code-analysis
    variables:
        SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
        GIT_STRATEGY: clone
    cache:
        key: "${CI_JOB_NAME}"
        paths:
        - .sonar/cache
    before_script:
        - apk add --update --no-cache curl
    script:
        - cd ${RECOMMENDATION_SERVICE_PATH}
        - |
            sonar-scanner -Dsonar.token=${RECOMMENDATION_SONAR_TOKEN} -Dsonar.projectKey=${RECOMMENDATION_SONAR_PROJECT_KEY} \
                -Dsonar.host.url=${SONAR_HOST_URL} -Dsonar.qualitygate.wait=true -Dsonar.projectName=${RECOMMENDATION_SERVICE_NAME}
    after_script:
        - |
          curl -u "${SONAR_USR_API_TOKEN}:" \
            -X GET "${SONAR_REPORT_URL}?key=${RECOMMENDATION_SONAR_PROJECT_KEY}&author=${GITLAB_USER_EMAIL}&token=${SONAR_USR_API_TOKEN}" -o ${SONAR_REPORT}
        - curl -u "${JFROG_USR}:${JFROG_PSW}" -T ${SONAR_REPORT} ${JFROG_PROJECT_SNAPSHOT}/sonarqube/
    artifacts:
        name: Sonarqube scan
        paths:
          - ${SONAR_REPORT}
        expire_in: 1 day
    timeout: 10min
    allow_failure: true
    interruptible: true
    tags:
        - shopping-app-docker

Snyk_Scan_Recommendationservice:
    image: node:alpine3.19
    stage: composition-analysis
    variables:
        GIT_STRATEGY: clone
    before_script:
        - apk add --no-cache openjdk11 unzip curl
        - npm install -g snyk snyk-to-html
    script:
        - cd ${RECOMMENDATION_SERVICE_PATH}
        - snyk auth ${SNYK_USER_API}
        - snyk test --json | snyk-to-html -o ${SNYK_REPORT} || true
        - curl -u "${JFROG_USR}:${JFROG_PSW}" -T ${SNYK_REPORT} ${JFROG_PROJECT_SNAPSHOT}/snyk/
    artifacts:
        name: Snyk scan
        paths:
        - ${RECOMMENDATION_SERVICE_PATH}/${SNYK_REPORT}
        expire_in: 1 day
    timeout: 10min
    allow_failure: true
    interruptible: true
    needs: 
        - job: Sonarqube_Scan_Recommendationservice
    tags:
        - shopping-app-docker

Trivy_Scan_Recommendationservice:
    stage: image-scan
    image: docker:24.0.5
    services:
        - name: docker:24.0.5-dind
          alias: docker
    variables:
        GIT_STRATEGY: clone
        DOCKER_HOST: tcp://docker:2375/
        DOCKER_DRIVER: overlay2
        DOCKER_TLS_CERTDIR: ""
        TRIVY_NO_PROGRESS: "true"
        TRIVY_CACHE_DIR: ".trivycache/"
    before_script:
        - export TRIVY_VERSION=$(wget -qO - "https://api.github.com/repos/aquasecurity/trivy/releases/latest" | grep '"tag_name":' | sed -E 's/.*"v([^"]+)".*/\1/')
    script:
        - cd ${RECOMMENDATION_SERVICE_PATH}
        - wget --no-verbose https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz -O - | tar -zxvf -
        - docker buildx build -t ${FULL_IMAGE} .
        - ./trivy image --format template --template "@contrib/html.tpl" --report summary -o ${TRIVY_REPORT} ${FULL_IMAGE}
        - curl -u "${JFROG_USR}:${JFROG_PSW}" -T ${TRIVY_REPORT} ${JFROG_PROJECT_SNAPSHOT}/trivy/
    cache:
        paths:
            - .trivycache/
    artifacts:
        name: Trivy scan
        paths:
            - ${RECOMMENDATION_SERVICE_PATH}/${TRIVY_REPORT}
        expire_in: 1 day
    timeout: 10min
    needs: 
        - job: Snyk_Scan_Recommendationservice
    allow_failure: true
    interruptible: true
    tags:
        - shopping-app-docker

Release_Recommendationservice_Image_Internal:
  stage: release-internal
  variables:
    GIT_STRATEGY: clone
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - |
      mkdir -p /kaniko/.docker
      echo "{\"auths\":{\"${PORTUS_URL}\":{\"auth\":\"$(echo -n ${PORTUS_USR}:${PORTUS_PSW} | base64)\"}}}" > /kaniko/.docker/config.json
      /kaniko/executor --context "${CI_PROJECT_DIR}/${RECOMMENDATION_SERVICE_PATH}" --dockerfile "${CI_PROJECT_DIR}/${RECOMMENDATION_SERVICE_PATH}/Dockerfile" --destination "${FULL_IMAGE}" --skip-tls-verify
  timeout: 15min
  allow_failure: false
  interruptible: true
  when: manual
  needs:
    - job: Trivy_Scan_Recommendationservice
  tags:
    - shopping-app-docker

Deploy_Recommendationservice_Dev_Cluster:
  stage: deploy-dev-cluster
  variables:
    GIT_STRATEGY: clone
  before_script:
    - |
      if [[ $GITLAB_USER_NAME != "lth216" ]]; then
        echo "User $GITLAB_USER_NAME doesn't have permission to perform this action !"
        exit 1
      fi
      export RECOMMENDATION_IMAGE_NAME="${PORTUS_URL}/dev/${RECOMMENDATION_SERVICE_NAME}"
      export RECOMMENDATION_IMAGE_VERSION="${CI_COMMIT_SHORT_SHA}"
  script:
    - |
      cd ${HELM_CHART_PATH}
      helmfile apply -f helmfile.yaml -l service=${RECOMMENDATION_SERVICE_NAME}
  allow_failure: false
  interruptible: true
  timeout: 5min
  needs: 
    - job: Release_Recommendationservice_Image_Internal
  when: manual
  tags:
    - shopping-app-shell